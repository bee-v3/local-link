# LocalLink

The LocalLink application runs on two docker containers:

1. locallink-server_server
   - AKA the server
   - Defined in the `Dockerfile` in the `locallink-server` folder <br>
2. locallink-server_postgres
   - AKA the database
   - Image of a Postgres database with the PostGIS extention installed, pulled from `mdillon/postgis` on [dockerhub](https://hub.docker.com/r/mdillon/postgis/)

# To Build and Run

The services makeing up the application are defined in `docker-compose.yml` so they can be run together in an isolated environment.

1. Docker Compose is needed to run the application, Compose is included with Docker Desktop which can be downloaded [here](https://www.docker.com/products/docker-desktop). Create an account with Docker Hub to allow Docker to pull required images.

2. From the root of the project directoy enter the command below and Compose will start and run the entire app.

```bash
$ docker-compose up
```

# How to access the website

Once both containers are running, the website can be accessed through a browser on your local machine at [http://localhost](http://localhost).

# How to SSH into a container

1. Use `docker ps` to get the name of the existing container you want to SSH into
2. Use the command `docker exec -it <container name> /bin/bash` to get a bash shell in the container

**Note:** If using VScode, working with containers is made easy with the [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) extension and the [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) extension pack.

# Account Generator CLI

1. Attach your terminal shell to the running server container, either though SSH or the extentions noted above.
2. Navigate to the locallinkapp/ directory.
3. Enter the command `node cli` and follow the prompts.

**A working work in progress**

# How to query the database from the command line

1. Attach your terminal shell to the running database container, either though SSH or the extentions noted above.
2. Enter `psql -d locallink_db -U silver` to open the PostgreSQL interactive terminal.

At this point you are connected to the database as user `silver` and any PostgreSQL quearys can be made. Helpful psql commands can be found [here](https://www.postgresqltutorial.com/psql-commands/) or [here](https://www.postgresql.org/docs/9.2/app-psql.html).

# How to remove all containers, images and database files and re-build everything from master.

**Warning this will remove _ALL_ docker containers. If you are using Docker containers for other projects more information about pruning containers can be found [here](https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes).**

From the root directory of the project

1. Ensure you are on the master branch with `git checkout master`
2. Enter the command `docker container stop $(docker container ls -aq)` to stop all running containers.
3. Enter the command `docker system prune -a` to remove all containers, images, build cache and networks that are not in use.
4. Enter the command `rm -r database` to delete the database files.
5. Enter the command `git pull`, to get the lastest version of master.
6. Enter the command `docker-compose up` to build and run.
