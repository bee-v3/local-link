var { users, location, job_post, worker } = require("./acc");
var crypto = require("crypto"); /* used for password hashing */
var uniqid = require("uniqid"); /* used to generate unique id's */
const geocoder = require("./geocoder");

function hashPW(pwd) {
  return crypto
    .createHash("sha256")
    .update(pwd)
    .digest("base64")
    .toString();
}

async function basic_account() {
  let account = {};

  /* profile table */
  username = uniqid();
  password = hashPW("silver");
  fname = username.slice(0, 7);
  lname = username.slice(7);
  email = fname + "@gmail.com";
  phone = Math.floor(Math.random() * 999999999) + 1000000000;
  about_msg = `Hello, my name is ${fname} ${lname}. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum`;
  is_worker = false;
  is_privet = false;
  post_history = null;
  pic_id = null;

  /* location table */
  latitude = (Math.random() * .75 + 36).toString().slice(0, 9); // random location in arbitrary area over va
  longitude = (-Math.random() * .75 - 76).toString().slice(0, 9);

  const loc_obj = await geocoder.reverse({ lat: latitude, lon: longitude }); // get address

  var address;
  if (loc_obj[0].hasOwnProperty("streetNumber")) {
    address = loc_obj[0].streetNumber + " " + loc_obj[0].streetName;
  } else {
    address = loc_obj[0].streetName;
  }
  city = loc_obj[0].city;
  state = loc_obj[0].administrativeLevels.level1short;
  zip = loc_obj[0].zipcode;
  coordinates = `POINT(${loc_obj[0].longitude} ${loc_obj[0].latitude})`; // returned coodinates match address better

  account.users = new users(
    username,
    password,
    fname,
    lname,
    email,
    phone,
    about_msg,
    is_worker,
    is_privet,
    post_history,
    pic_id
  );
  account.location = new location(
    username,
    address,
    city,
    state,
    zip,
    coordinates
  );
  return account;
}

async function account_with_post() {
  let account = await basic_account();

  let skills_array = [
    "Carpentry",
    "Cleaning",
    "Electrical",
    "Lawn care",
    "Landscaping",
    "Painting",
    "Plumbing",
    "Trash Removal",
    "Welding",
    "Moving",
    "Drywall repair"
  ];
  skills = [
    skills_array[Math.floor(Math.random() * skills_array.length)],
    skills_array[Math.floor(Math.random() * skills_array.length)]
  ];
  job_id = uniqid("job");
  poster_id = account.users.username;
  worker_id = null;
  post_date = new Date();
  completed_date = null;
  price = Math.floor(Math.random() * 140) + 10;
  // view_rad = Math.floor(Math.random() * 10) + 1;
  view_rad = 10;
  job_desc = `My name is ${account.users.fname} ${account.users.lname}, I live in ${account.location.city}. I am in search of someone with experience in ${skills[0]} and ${skills[1]} to help me out around my house. If this sounds like you, contact me throught the chat. I am willing to pay $${price}.`;
  chat_ids = null;
  is_privet = false;
  jobCategories = skills; //equivlent for now
  title = `Looking for ${skills[0]} help, $${price}`;

  account.users.post_history = [job_id];
  account.job_post = new job_post(
    job_id,
    poster_id,
    worker_id,
    post_date,
    completed_date,
    job_desc,
    price,
    chat_ids,
    is_privet,
    view_rad,
    skills,
    jobCategories,
    title
  );
  return account;
}

async function worker_account() {
  let account = await basic_account();

  account.users.is_worker = true;
  user_id = account.users.username;
  payrate = Math.floor(Math.random() * 140) + 10;
  avgRating = (Math.random() * 5);
  job_hist = null;
  work_rad = 10;

  let skills_array = [
    "Carpentry",
    "Cleaning",
    "Electrical",
    "Lawn care",
    "Landscaping",
    "Painting",
    "Plumbing",
    "Trash Removal",
    "Welding",
    "Moving",
    "Drywall repair"
  ];
  skills = [
    skills_array[Math.floor(Math.random() * skills_array.length)],
    skills_array[Math.floor(Math.random() * skills_array.length)]
  ];
  jobCategories = skills;

  account.worker = new worker(
    user_id,
    skills,
    payrate,
    job_hist,
    work_rad,
    jobCategories,
    avgRating
  );
  return account;
}

module.exports = {
  basic_account,
  account_with_post,
  worker_account
};
