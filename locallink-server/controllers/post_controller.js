const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");

module.exports = {
  async get10() {
    const posts = await pool.query(pgquery.query10);
    return posts.rows;
  },
  async getJob(job_id) {
    const job_post = await pool.query(pgquery.getJob, [job_id]);
    return job_post.rows[0];
  },
  async jobSearchSpec(username, category, distance) {
    if(category == 'All'){
      category = [
        "Carpentry",
        "Cleaning",
        "Electrical",
        "Lawn care",
        "Landscaping",
        "Painting",
        "Plumbing",
        "Trash Removal",
        "Welding",
        "Moving",
        "Drywall repair"
      ];
    }else{
      category = [category];
    }
    const job_post = await pool.query(pgquery.jobSearchSpec, [username, category, distance]);
    return job_post.rows;
  },
  async jobSearchDefault(username) {
    const job_post = await pool.query(pgquery.jobSearchDefault, [username]);
    return job_post.rows;
  },

  async getLocation(username) {
    const location = await pool.query(pgquery.getLoc, [username]);
    let spt = location.rows[0].st_astext.split(' ');
    let ypoint = spt[0].substr(6);
    let xpoint = spt[1].substr(0, spt[1].indexOf(')')-1);
    let loc = {x:xpoint, y:ypoint};
    return loc;
  }
};