const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");

exports.editprofile = async (req, res) => {
  if (req.session.username == null){
	res.redirect("/");
  }
  else{ 
    // Pull a client from the pool. Needed for databse transaction
    const client = await pool.connect();  
    let address = req.body.str_address + " " + req.body.road; 
    try{      
      // Start transaction. Commands are rolled back if any step is unsucessful
      await client.query('BEGIN');
      await client.query(pgquery.updateuserinfo, [req.session.username, req.body.email, req.body.phone, req.body.about_msg]);
      await client.query(pgquery.updateuserlocation, [req.session.username, address, req.body.city, req.body.state, req.body.zip, `POINT(${req.body.lng} ${req.body.lat})`]);
      await client.query('COMMIT');
      req.session.email = req.body.email;
      req.session.phone = req.body.phone;
      req.session.about_msg = req.body.about;
      req.session.address = address;
      req.session.city = req.body.city;
      req.session.state = req.body.state;
      req.session.zip = req.body.zip;
      req.session.save();
      res.redirect("/");
    } catch (err) {
      await client.query('ROLLBACK');      
      console.log('Job accept transaction failed!');
      res.redirect("/");
    } finally {
      client.release();   // Return client when done      
    }
  }
}