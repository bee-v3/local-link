const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");

exports.workerpost = async function(req, res){
  if (req.session.username == null){
    req.session.msg = "You must be logged in to post a job. Please login.";
    res.redirect('/login');
  }
  else{
    let workerDetails = [req.session.username,req.body.skillCategory,req.body.payrate,req.body.workRadius,req.body.workCategory, 3.000];
    try{
  
      await pool.query(pgquery.postworker, workerDetails)
      req.session.msg = 'Your worker profile has been created!';
      res.redirect('/postsuccess');
      console.log('Worker profile Added to database');     
      
    } catch (err) {
      req.session.msg = "You must be logged in to post a job. Please login.";
      res.redirect('/login');
    }
  }
};
