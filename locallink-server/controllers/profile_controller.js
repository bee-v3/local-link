const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");

exports.getjobs = async function (req) {
  if (typeof req.session.username == 'undefined') {
    res.redirect('/login');
  }
  else {
    try {
      let myJobs = await pool.query(pgquery.allMyJobs, [req.session.username]);
      return myJobs.rows;      
    } catch (err) {
      return null;
    }
  }
};
