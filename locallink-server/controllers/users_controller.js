/* require() imports the nodejs modules and middleware */
var fs = require("fs-extra"); /* provides file system functions */
var crypto = require("crypto"); /* used for password hashing */
/* mongoose is middleware used for assessing a MongoDB data base */
var pool = require("../lib/postgres");
var pgquery = require("../lib/dbqueries");
/* Multer is a node.js middleware for handling multipart/form- 
 data, which is primarily used for uploading files. It adds a body object and a file
or files object to the request object. The body object contains the values of the text 
fields of the form, the file or files object contains the files uploaded via the form. */
var multer = require("multer");

/* global variables */
var dateValue = "";
var commentsValue = "";
var imgPath = "./uploads/tmp.jpg";

/* multers disk storage settings */
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    cb(
      null,
      "tmp" +
        "." +
        file.originalname.split(".")[file.originalname.split(".").length - 1]
    );
  }
});

/* multer settings */
var upload = multer({ storage: storage }).single("file");

/* adds new documents to the LocalLink collection and saves uploaded image to the 
public/images/ directory */
/*
exports.addDocToLocalLink = function(req, res){
var datetimestamp = Date.now();
var fileName = '/images/' + req.session.username + '/' + datetimestamp + '.jpg';
var locallink = new LocalLink({creator_id : req.session.user, 
fileName: fileName, date: req.body.date, comments: req.body.comments });
var __dirname_str = __dirname;
var end = __dirname_str.length;
end = end - 11;
__dirname_str =  __dirname_str.substr(0, end);
var dir = __dirname_str  + 'public/images/' + req.session.username ;
if (!fs.existsSync(dir))
{ fs.mkdirSync(dir); }

 
   var file;
   fs.readFile(imgPath, null, function read(err, data) {
    if (err) {throw err;}

    //console.log(data);
    
   var path = "./public" + fileName;
   fs.writeFile(path, data, function (err) {
    if (err) throw err;
    });    
  });

 
  locallink.save(function(err) {
    if (err){
      res.session.error = err;
       res.redirect('/');
    } else {
        res.redirect('/');
    }
  });   
 };
 */

//used to load pictures to the public directory
/*
exports.uploader = function(req, res) {
        //console.log("in uploader");
        upload(req,res,function(err){
            if(err){
                 res.json({error_code:1,err_desc:err});
                 return;
            }
              
              res.json({error_code:0,err_desc:null});
              
        });
    };
*/

/* returns a json containing all database documents, "/.*./i" represents an empty string */
/*
exports.searchLocalLink_db = function(req, res) {  
//console.log("In searchUserLog");    
if (! dateValue)
dateValue =/.i;

if (! commentsValue)
commentsValue =/.i;

//returns json containing all documents in the database for the logged in user which match the regular express commentsValues 
LocalLink.find({creator_id: req.session.user, comments: {$regex:commentsValue}}).exec(function(err, user) {
  
 // console.log(user);   
   
   res.json(user);
    });  
}; 
*/

/* adds new user to the User collection */
exports.signup = async function(req, res) {
  if (req.body.username === null || req.body.password === null) {
    res.session.msg = "Could not complete signup. Please try again.";
    res.redirect("/signup");
  } else {
    let date_ob = new Date();
    let hashed_password = hashPW(req.body.password);
    let userinformation = [
      req.body.username,
      hashed_password,
      req.body.fname,
      req.body.lname,
      req.body.email,
      req.body.phone,
      req.body.about
    ];
    // console.log(crypto.randomBytes(16).toString(req.body.username + date_ob));
    try {
      await pool.pgPool.query(pgquery.registeruser, userinformation);
      //req.session.userid = userinformation[2];
      req.session.username = userinformation[0];
      /*req.session.msg =
        "Signed up as " + userinformation[0] + ", please log in.";*/
      res.redirect("/");
      console.log(userinformation[0] + " registered!");
    } catch (err) {
      //res.session.err = err;
      console.log(err);
      req.session.msg = "Could not complete signup. Please try again.";
      console.log(
        "Error signing up user as " +
          req.body.username +
          " with password " +
          req.body.password
      );
      res.redirect("/signup");
    }
  }
};

/* uses User collection to authenticate users */
exports.login = async function login(req, res) {
  if (req.body.username === null || req.body.password === null) {
    res.session.msg = "Could not complete login. Please try again.";
    res.redirect("/login");
  } else {
    let hashed_password = hashPW(req.body.password);
    let userinformation = [req.body.username, hashed_password];

    try {
      let result = await pool.pgPool.query(pgquery.loginuser, [
        userinformation[0]
      ]);

      if (hashed_password !== result.rows[0].password) {
        console.log(
          "Error logging in user as " +
            req.body.username +
            " with password " +
            req.body.password +
            ", wrong password"
        );
        res.redirect("/login");
      } else {
        req.session.username = userinformation[0];
        req.session.msg = "Authenticated as " + userinformation[0];
        res.redirect("/");
        console.log(userinformation[0] + " logged in!");
      }
    } catch (err) {
      req.session.msg = "Could not complete login. Please try again.";
      console.log(
        "Error logging in user as " +
          req.body.username +
          " with password " +
          req.body.password +
          "because of error"
      );
      console.log(err);
      res.redirect("/login");
    }
  }
};

exports.jobpost = async function(req, res) {
  if (req.session.username == null) {
    res.session.msg = "You must be logged in to post a job. Please login.";
    res.redirect("/login");
  } else {
    let date_ob = new Date();
    let jobid = uniqid("job");
    let jobpost = [
      jobid,
      req.session.username,
      date_ob,
      req.body.description,
      req.body.price,
      req.body.radius
    ];
    try {
      await pool.pgPool.query(pgquery.postjob, jobpost);
      req.session.msg = "Your job has been posted!";
      res.redirect("/postsuccess");
      console.log("Job posted");
    } catch (err) {
      res.session.msg = "You must be logged in to post a job. Please login.";
      res.redirect("/login");
    }
  }
};

exports.workerpost = async function(req, res) {
  if (req.session.username == null) {
    res.session.msg = "You must be logged in to post a job. Please login.";
    res.redirect("/login");
  } else {
    let workerDetails = [
      req.session.username,
      req.body.skillDescription,
      req.body.payrate,
      req.body.workRadius,
      req.body.workCategory
    ];
    try {
      await pool.pgPool.query(pgquery.postworker, workerDetails);
      req.session.msg = "Your worker profile has been created!";
      res.redirect("/postsuccess");
      console.log("Worker profile Added to database");
    } catch (err) {
      res.session.msg = "You must be logged in to post a job. Please login.";
      res.redirect("/login");
    }
  }
};

exports.profile = async function(req, res) {
  if (!req.session.username) {
    res.session.msg = "You must be logged in to post a job. Please login.";
    res.redirect("/login");
  } else {
    try {
      let result = await pool.pgPool.query(pgquery.getProfile, [
        req.session.username
      ]);
      console.log("Profile fetched successfully!");
      req.session.fname = result.rows[0].fname;
      req.session.lname = result.rows[0].lname;
      req.session.email = result.rows[0].email;
      req.session.phone = result.rows[0].phone;
      req.session.about_msg = result.rows[0].about_msg;
      req.session.is_worker = result.rows[0].is_worker;
      req.session.is_privet = result.rows[0].is_privet;
      req.session.post_history = result.rows[0].post_history;
      req.session.pic_id = result.rows[0].pic_id;

      res.redirect("/profile");
    } catch (err) {
      req.session.msg = "You must be logged in to post a job. Please login.";
      res.redirect("/login");
    }
  }
};
