module.exports = function(io){
	var pool = require("../lib/postgres");
	var pgquery = require("../lib/dbqueries");

	const chat = async (req, res) => {
		let author = req.session.username;
		let toUser = req.params.toUser;
		res.render("chat", {user: toUser});
		var rooms = [];
		var roomIDs = [];
		io.on('connection',function(socket){
			var roomID = generateRoomId([req.session.username, toUser]);
			socket.join(roomID);
			console.log(req.session.username+" joined the room "+roomID);
			if(!roomIDs.includes(roomID)){
				let room = new chatroom(roomID);
				room.users = [req.session.username, toUser];
				rooms.push(room);
				roomIDs.push(roomID);
			}

			socket.on('sendMessage',function(data){
				socket.broadcast.emit('New Message',{msg: data.msg, author: req.session.username, time: data.datetime});
			});
		});
		
	}

	return{chat:chat}
}


function generateRoomId(arr){
	arr = arr.sort();
	var id = "";
	arr.forEach(function(str,i){
		id += str;
	});
	return id;
}

function chatroom(roomID){
	this.ID = roomID;
	this.users = [];
	this.logs = [];
}

function chatlog(msg, from, ts){
	this.text = msg;
	this.author = from;
	this.timestamp = ts;
}

/*exports.sendMessage = async function(req, res) {

}

exports.newMessage = async function(req, res) {

}*/