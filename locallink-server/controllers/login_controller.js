const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");
var hashPW = require("../lib/hashpw");

/* uses User collection to authenticate users */
exports.login = async function login(req, res) {
    if (req.body.username === null || req.body.password === null) {
      res.session.msg = "Could not complete login. Please try again.";
      res.redirect("/login");
    } else {
      var hashed_password = hashPW(req.body.password);
      var userinformation = [req.body.username, hashed_password];
  
      try {
        var result = await pool.query(pgquery.loginuser, [userinformation[0]]);
        
        if (hashed_password !== result.rows[0].password) {
          console.log("Error logging in user as " + req.body.username + " with password " + req.body.password + ", wrong password");
          res.redirect("/login");
        } else {
          var location_result = await pool.query(pgquery.loc, [userinformation[0]]);
          var worker_result = await pool.query(pgquery.getworkerinfo, [userinformation[0]]);
          req.session.username = userinformation[0];
          req.session.msg = "Authenticated as " + userinformation[0];
          req.session.fname = result.rows[0].fname;
          req.session.lname = result.rows[0].lname;
          req.session.about_msg = result.rows[0].about_msg;
          req.session.email = result.rows[0].email;
          req.session.phone = result.rows[0].phone;
          req.session.address = location_result.rows[0].address;
          req.session.city = location_result.rows[0].city;
          req.session.state = location_result.rows[0].state;
          req.session.zip = location_result.rows[0].zip;
          req.session.post_history = result.rows[0].post_history;
          //req.session.job_hist = worker_result.rows[0].job_hist;
          req.session.save();
          console.log(userinformation[0] + " logged in!");
          res.redirect("/");
        }
      } catch (err) {
        req.session.msg = "Could not complete login. Please try again.";
        console.log("Error logging in user as " + req.body.username + " with password " + req.body.password + "because of error");
        console.log(err);
        res.redirect("/login");
      }
    }
  };