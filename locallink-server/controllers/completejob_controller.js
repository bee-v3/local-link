const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");

exports.completejob = async (message, job_id) => {
  console.log(message);
  if (job_id){
    try{
      if(message){
        await pool.query(pgquery.complreq, [message, job_id]);
      } else{
        await pool.query(pgquery.complreq, [" ",job_id]);
      }
      var response_success = {accepted: 1, msg: 'Completion request sent!'};
      return response_success;
    } catch (err) {      
      console.log(err);
      var response_failed_db = {accepted: 0, msg: 'Error sending completion request: Database query error.'};
      return response_failed_db;
    }
  }
  else{ 
    var response_failed_db = {accepted: 0, msg: 'Error sending completion request: No job id.'};
    return response_failed_db;
  }
}

exports.completejob_response= async (message, response, job_id) => {
  console.log(message);
  if (job_id){
    var resp = (response == 'true') ? true : false;
    try{
      if(resp){
        if(message){await pool.query(pgquery.complresp1, [message, job_id]);}
        else{await pool.query(pgquery.complresp1, [" ", job_id]);}
      } else{
        if(message){await pool.query(pgquery.complresp2, [message, job_id]);}
        else{await pool.query(pgquery.complresp2, [" ", job_id]);}
      }
      var response_success = {accepted: 1, msg: 'Completion response request sent!'};
      return response_success;
    } catch (err) {      
      console.log(err);
      var response_failed_db = {accepted: 0, msg: 'Error sending completion response request: Database query error.'};
      return response_failed_db;
    }
  }
  else{ 
    var response_failed_db = {accepted: 0, msg: 'Error sending completion response request: No job id.'};
    return response_failed_db;
  }
}
