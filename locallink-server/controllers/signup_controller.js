const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");
var hashPW = require("../lib/hashpw");

exports.signup = async function(req, res) {
    if(req.session.msg){
      delete req.session.msg;
    }
    let hashed_password = hashPW(req.body.password);
    let userinformation = [req.body.username, hashed_password, req.body.fname, req.body.lname, req.body.email, req.body.phone, req.body.about];
    let address = req.body.str_address + " " + req.body.road;
    let loc_obj = [req.body.username, address, req.body.city, req.body.state, req.body.zip, `POINT(${req.body.lng} ${req.body.lat})`];
    const client = await pool.connect();     
    try {
      await Promise.all([
        client.query('BEGIN'),
        client.query(pgquery.registeruser, userinformation),
        client.query(pgquery.createworker, [userinformation[0]]),
        client.query(pgquery.insertLoc, loc_obj)
      ]);
      await client.query('COMMIT');
     
      req.session.username = userinformation[0];
      req.session.fname = req.body.fname;
      req.session.lname = req.body.lname;
      req.session.email = req.body.email;
	    req.session.phone = req.body.phone;
  	  req.session.about_msg = req.body.about;
  	  req.session.address = address;
	    req.session.city = req.body.city;
	    req.session.state = req.body.state;
      req.session.zip = req.body.zip;
      req.session.job_hist = [];
      req.session.post_history = [];
      req.session.save();
      res.redirect("/");
      console.log(userinformation[0] + " registered!");
    } catch (err) {
      try{
        await client.query('ROLLBACK')
        client.release();
      }catch (rollbackErr) {
        // Rollback failed so discard connection
        client.release(rollbackErr);
      }
      console.log(err);
      req.session.msg = "Could not complete signup. Please try again.";
      res.redirect("/signup");
      console.log("Error signing up user as " + req.body.username + " with password " + req.body.password);
    } 
}