const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");
var uniqid = require("uniqid"); /* used to generate unique jobid's */

exports.jobpost = async function(req, res){
  if (req.session.username == null){
    res.session.msg = "You must be logged in to post a job. Please login.";
    res.redirect('/login');
  }
  else{
    const client = await pool.connect();
    var date_ob = new Date();
    var jobid = uniqid("job");
    var need_photo = (req.body.needs_compl_photo === "false") ? false : true;
    var jobpost = [jobid, req.session.username, date_ob, req.body.description, req.body.price, req.body.radius, req.body.title, req.body.job_category, req.body.job_category, need_photo];
    try{
      await client.query('BEGIN');
      await client.query(pgquery.postjob, jobpost);
      await client.query(pgquery.updatePostHistory, [req.session.username, jobid]);
      await client.query('COMMIT');
      res.redirect('/postsuccess');
      console.log('Job posted');           
    } catch (err) {
      await client.query('ROLLBACK');
      res.redirect('/login');
      console.log('Job post failed');
      console.log(err);
    } finally {
      client.release();   // Return client when done      
    }
  }
};