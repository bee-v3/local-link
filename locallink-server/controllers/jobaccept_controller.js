const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");

exports.jobaccept = async (username, job_id) => {
  if (username == null || job_id == null){
    var response_failed_notlogged = {"accepted": "0", "msg": "Error accepting job: Invalid user/job id!"};
    return response_failed_notlogged;
  }
  else{     
    // Pull a client from the pool. Needed for databse transaction
    const client = await pool.connect();   
    try{      
      // Start transaction. Commands are rolled back if any step is unsucessful
      await client.query('BEGIN');
      await client.query(pgquery.acceptjob_step1, [username, job_id]);
      await client.query(pgquery.acceptjob_step2, [username, job_id]);
      await client.query(pgquery.acceptjob_step3, [job_id]);
      await client.query('COMMIT');
      console.log('Job accept transaction worked!');
      var response_success = {accepted: 1, msg: 'Job accepted!'};
      return response_success;
    } catch (err) {
      await client.query('ROLLBACK');      
      console.log('Job accept transaction failed!');
      var response_failed_db = {accepted: 0, msg: 'Error accepting job: Database query error.'};
      return response_failed_db;
    } finally {
      client.release();   // Return client when done      
    }
  }
}