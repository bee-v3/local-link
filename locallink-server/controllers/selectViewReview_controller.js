/* require() imports the nodejs modules and middleware */
/* postgres is middleware used for assessing a Postgres data base */
var pool = require("../lib/postgres");
var pgquery = require("../lib/dbqueries");

/* selects user review from review table */
exports.selectReview = async function(req, res) {
  if (req.session.username == null){
    req.session.msg = "You must be logged in to post a review. Please login.";
    res.redirect('/login'); 
  } else {
           let reviewinformation = [req.body.username];
    try {
           let reviewResult = await pool.pgPool.query(pgquery.selectReview , reviewinformation);
           //res.redirect("/viewReview");
    } catch (err) {
      //res.session.err = err;
      console.log(err);
      req.session.msg = "Could not create user review. Please try again.";
      res.redirect("/");
    }
  }
};

/* displays user review from review table */
exports.viewReview = async function(username, res) {
  if (username == null){
    return {}; 
  } else {
    try {
      reviewResult = await pool.pgPool.query(pgquery.selectReview , [username]);
      return reviewResult    //    res.render('viewReview', {arr: reviewResult.rows, user: reviewinformation}); 
    } catch (err) {
      //res.session.err = err;
      console.log(err);
      console.log("Could not create user review. Please try again.");
      return {};
      //res.redirect("/");
    }
  }
};