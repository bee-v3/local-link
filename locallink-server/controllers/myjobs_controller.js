var pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");


exports.myActiveJobs = async function (req) {
    if (req.session.username == null) {
        res.redirect('/login');
    } else {
        try {
            let activeJobs = await pool.query(pgquery.allMyActiveJobs, [req.session.username]);
            if (activeJobs.rows.length > 0) {
                return activeJobs.rows;
            }
            else {
                activeJobs = '0';
                return activeJobs;
            }


        } catch (err) {
            console.log(err);
            req.session.msg = "No active jobs found.";
            res.redirect("/");
        }
    }
};

exports.myActiveWorking = async function (req) {
    if (req.session.username == null) {
        res.redirect('/login');
    } else {
        try {
            let activeWorking = await pool.query(pgquery.allMyActiveWorking, [req.session.username]);
            if (activeWorking.rows.length > 0) {
                return activeWorking.rows;
            }
            else {
                activeWorking = '0';
                return activeWorking;
            }


        } catch (err) {
            console.log(err);
            req.session.msg = "No active jobs found.";
            res.redirect("/");
        }
    }
};

exports.myPastPosted = async function (req) {
    if (req.session.username == null) {
        res.redirect('/login');
    } else {
        try {
            let pastPosted = await pool.query(pgquery.allMyPastPosted, [req.session.username]);
            if (pastPosted.rows.length > 0) {
                return pastPosted.rows;
            }
            else {
                pastPosted = '0';
                return pastPosted;
            }


        } catch (err) {
            console.log(err);
            req.session.msg = "Something went wrong.";
            res.redirect("/");
        }
    }
};

exports.myPastWorked = async function (req) {
    if (req.session.username == null) {
        res.redirect('/login');
    } else {
        try {
            let pastWorked = await pool.query(pgquery.allMyPastWorked, [req.session.username]);
            if (pastWorked.rows.length > 0) {
                return pastWorked.rows;
            }
            else {
                pastWorked = '0';
                return pastWorked;
            }


        } catch (err) {
            console.log(err);
            req.session.msg = "Something went wrong.";
            res.redirect("/");
        }
    }
};


exports.deleteMyJob = async function (req, res) {
    try {
      console.log('Deleting Job: ' + req.body.job_id);
      let jobToDelete = [req.body.job_id];
      let userCheck = await pool.query(pgquery.getJob, jobToDelete );
      if (userCheck.rows[0].poster_id != req.session.username){
          req.session.msg = "You may not delete jobs you did not post";
          res.redirect('back');
      }
      else{
        await pool.query(pgquery.deleteJob, jobToDelete);
        res.redirect('back');
      }

  
    }
    catch (err) {
      req.session.msg = "Could not delete job.";
      res.status(400);
    }
  };