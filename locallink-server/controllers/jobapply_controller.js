const pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");

exports.jobapply = async (username, job_id) => {
  if (username == null || job_id == null){
    var response_failed_notlogged = {"accepted": "0", "msg": "Error applying for job: Invalid user/job id!"};
    return response_failed_notlogged;
  }
  else{ 
    try{                
      await pool.query(pgquery.applyjob, [username, job_id]);
      var response_success = {accepted: 1, msg: 'Job application sent!'};
      return response_success;
    } catch (err) {      
      var response_failed_db = {accepted: 0, msg: 'Error applying for job: Database query error.'};
      return response_failed_db;
    }
  }
}