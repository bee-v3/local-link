var pool = require("../lib/postgres").pgPool;
var pgquery = require("../lib/dbqueries");

getAllWorkers = `
SELECT * from worker
WHERE payrate IS NOT NULL 
`;

module.exports = {
  async getAll() {
    const workers = await pool.query(getAllWorkers);
    return workers.rows;
  },

  /* This function matches workers to a job poster based on their most recent job post. The function looks for workers
  that contain the skills required to do the job posters most recent job. After matching the skills, the function
  searches for workers with an average rating greater than 3. Once the workers with a high average ratings are found
  the function picks workers that are within the price range of the job posters most recent job post. The function
  sorts the remaining works by average rating and chooses the top 3. */
  async matchWorkers(req) {
    try {
      //Find the job poster's most recent job
      let result = await pool.query(pgquery.findRecentJob, [req.session.username]);
      //If no jobs are posted, just match the 3 highest rated workers
      if (result.rows.length == 0){
        if(req.session.username == null){
          matchedWorkers = await pool.query(pgquery.findWorkersNoJobs, ['null']);
          return matchedWorkers.rows;
        }else{
          matchedWorkers = await pool.query(pgquery.findWorkersNoJobs, [req.session.username]);
          return matchedWorkers.rows;
        }
      }
      //Get the price of the job poster's most recent job
        var price = result.rows[0].price;
        var intParse = parseInt(price,10);
        //Makes it so the match will match workers that charge a little more than what the job poster's job payout is
        var intParse = intParse + 10;
        //Get the skills required for the job poster's most recent job
        var skills = [result.rows[0].skills];
        let queryData = [req.session.username, intParse, skills];
        //Find the workers
        matchedWorkers = await pool.query(pgquery.findWorkers, queryData);
        return matchedWorkers.rows; 
    } catch (err) {
      console.log(err);
    }
  },
  async workerSearch(username, category, distance, price) {
    if(category == 'All'){
      category = [
        "Carpentry",
        "Cleaning",
        "Electrical",
        "Lawn care",
        "Landscaping",
        "Painting",
        "Plumbing",
        "Trash Removal",
        "Welding",
        "Moving",
        "Drywall repair"
      ];
    }else{
      category = [category];
    }
    const workers = await pool.query(pgquery.workerSearch, [username, category, distance, price]);
    return workers.rows;
  }

}
