/* require() imports the nodejs modules and middleware */
var uniqid = require("uniqid"); /* used to generate unique jobid's */
/* postres is middleware used for assessing Postgres data base */
var pool = require("../lib/postgres");
var pgquery = require("../lib/dbqueries");
var avgSum = null;
var newAvgrating = null;


/* adds new user review to the review table */
exports.createReview = async function(req, res) {
  if (req.session.username == null){
    res.session.msg = "You must be logged in to post a review. Please login.";
    res.redirect('/login'); 
  } else {
           let review_id = uniqid("rev");           
           let rev_date = new Date();            
           let job_ref = null;
           let reviewinformation = [review_id, req.body.username, req.session.username, req.body.review, req.body.rating, rev_date,    
           job_ref ];
           let workerName = [req.body.username];
    try {
            await pool.pgPool.query(pgquery.createReview , reviewinformation);
            //Checking worker table in database if this user is a worker
            let isWorker = await pool.pgPool.query(pgquery.workerCheck, workerName);
            //If the user is not found in the worker table, the user is not a worker and the user is redirected
            if (isWorker.rows.length == 0){
              res.redirect("/");
            }
            else{
              //Get all the ratings from the reviews table that matches the reviewed user's username
              let getRatings = await pool.pgPool.query(pgquery.findRatings, workerName);
              avgSum = 0.000;
              //Add all the ratings together and divide by the number of ratings to get the average
              for(var i = 0; i < getRatings.rows.length; i++){
                var floatParse = parseFloat(getRatings.rows[i].rating);
                avgSum = avgSum + floatParse;
              }
              newAvgrating = avgSum / getRatings.rows.length;
              let newRatingData = [newAvgrating, req.body.username];
              //Update the average rating of the worker and redirect to home page
              await pool.pgPool.query(pgquery.updateAvgrating, newRatingData);    
              res.redirect("/");
            }
    } catch (err) {
      //res.session.err = err;
      console.log(err);
      req.session.msg = "Could not create user review. Please try again.";
      res.redirect("/createReview");
    }
  }
};


