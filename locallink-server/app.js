/* Node.js is an open-source, cross-platform, runtime environment that is used to create a web server and allows 
developers to create server-side applications in JavaScript. Express is a popular Node web framework. It provides 
mechanisms to handel web requests and web responses, handel routes, and provides rendering engines that inserts 
data into html templates. It provides mechanisms to allow adding additional request processing "middleware" at any 
point within the request handling pipeline. "express-session" is middleware that stores a session identifier on the 
client within a cookie and stores the session data on the server. Express apps can use any database that is supported 
by Node, such as MongoDB which is an open source NoSQL database. */


/* require() imports the nodejs modules and middleware */
var createError = require('http-errors');
var express = require('express');
var socketio = require( "socket.io" );
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var pgSession = require('connect-pg-simple')(session);
var pool = require('./lib/postgres');
var fs = require('fs');

/// TODO: reimplement picture uploads
/* Multer is a node.js middleware for handling multipart/form-data, which is primarily used for uploading files. 
Multer will not process any form which is not multipart (multipart/form-data) */
/*
var multer = require('multer');
require('./models/users_model.js');
*/

/* arguments support */
var argv = require('minimist')(process.argv.slice(2));
var host = argv.host ? argv.host : 'localhost';
var port = argv.port ? argv.port : '80';
var proxy = argv.proxy ? argv.proxy.replace(/\/+$/, '') : ('http://' + host + (port == '80' ? '' : ':' + port));
var debugMode = argv.debug ? argv.debug : false;

var app = express();
var io = socketio();
app.io = io;

/* view engine setup, ejs allows for a html like interface*/
app.engine('.html', require('ejs').__express);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

/* configure Express application */
app.use(bodyParser.urlencoded({ extended: true })); //uses deep parsing to deal with nested objects (extended: true)
app.use(bodyParser.json()); //parse meassages using json format
app.use(cookieParser()); //used to parse the cookies into the req.cookies object
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// TODO: Set so pages show .html extension to emulate web server
app.use(express.static(path.join(__dirname, '/public')));


/* set and configure session */

// TODO: Figure out why letting this use Pool prevents other functions from using Pool
app.use(session({
  store: new pgSession({
    pool: pool.pgPool,
    tableName: 'session',
  }),
  secret: 'SECRET',
  cookie: {maxAge: 30 * 24 * 60 * 60 * 1000},
  saveUninitialized: false,
  resave: false
}));

/* Allow ejs templates to use req.session globally */

app.use(function reqToResLocals(req, res, next){
  res.locals.session = req.session;
  next();
});

/* tells Express what route files to use */
fs.readdir("./routes", (err, files) => {
  files.forEach(file => {
    if(file == "chat.js" || file == "searchworkers.js" || file == "searchjobs.js"){
      app.use("/", require("./routes/" + file)(io));
    }else{
      app.use("/", require("./routes/" + file));
    }
  });
  });


/* allows cross origin requests */
app.use(function(req, res, next) { //allow cross origin requests
        res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
        res.header("Access-Control-Allow-Origin", proxy);
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });


app.listen(port, '0.0.0.0', function(err) {
  if(err) {
    return console.log('Error occured: '+err);
  }
  console.log(`Listening on port ${port}`);
})

/**
* Handles exit events
*/
function exitHandler(options, exitCode) {
  pool.pgPool.end();
  if (options.cleanup) console.log('clean');
  if (exitCode || exitCode === 0) console.log(exitCode);
  if (options.exit) process.exit();
}

/**
* Graceful exit
*/
process.on('exit', exitHandler.bind(null,{cleanup:true}));

/**
* CTRL + C event exit
*/
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

/**
* "kill pid" exit
*/
process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));

/**
* Uncaught exception exit
*/
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));

/**
* Logs messages to console if debugMode is set to true
*
* @param str - string to log
*/
function consoleLogIfRequired(str) {
  if(debugMode) {
    console.log(str);
  }
}

module.exports = app;