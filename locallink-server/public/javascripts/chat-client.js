var socket = io();

function sendMessage(){
	let messageContent = $("#message_input").val();
	let time = new Date().toLocaleString();
	socket.emit("sendMessage", {msg: messageContent, datetime:time} );
	$("#chatlog").append(`<div class="message"><span class="author">You - </span><span class="datetime">${time}</span><br><div class="text">${messageContent}</div></div><br><br>`);
	$("#message_input").val("");
}

socket.on('New Message', function(data){
	$("#chatlog").append(`<div class="from-message"><span class="author">${data.author} - </span><span class="datetime">${data.time}</span><br><div class="text">${data.msg}</div></div><br><br>`);
});