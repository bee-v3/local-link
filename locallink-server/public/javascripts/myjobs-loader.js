var i = 0;


function start() {
  fetchCurrentJobs();
}

/**
* Example function to simulate fetching user data from the database
* and dynamically updating the HTML. Updates the progress bar on the
* front-end as everything loads.
*
* Runs as soon as the document loads as the jobs dashboard should be
* the first thing the user sees.
**/
function fetchCurrentJobs() {

  setTimeout(removeLoader, 2000);
}

function removeLoader() {
  $("#inprogress-container").show();
  $(".load-container").hide();
  $("#actively-working-container").show();
  $(".load-container").hide();
  $("#past-posted-container").show();
  $(".load-container").hide();
  $("#past-worked-container").show();
  $(".load-container").hide();
}