/* The $http service is a core AngularJS service that facilitates communication with the web server via the 
browser's HttpRequest objects.  Reference to the browser's window object in AngularJS we always refer to it 
through the $window service, so it may be overridden, removed or mocked for testing */



angular.module('searchApp', []).
  controller('searchController', ['$scope','$http','$window',  
                              function($scope, $http, $window ) {
    
    
    var recordCount = 1;
    var test;
    $http.get('/user/Search')
        .then(function(response) {
      $scope.array = response.data;
      $scope.user =  $scope.array[0] ;
      test =  $scope.array[0].fileName; 
      $scope.error = "";
       //var filename = "/images/" + $scope.user.timeStamp + ".jpg";
       //var img = "/images/1546951706254.jpg";
      // $scope.image = img;               
     }).
    catch(function(response) {
      $scope.user = {};
      $scope.error = data;
    });
     

    $scope.updateWorklog = function(data) { 
       
     var data = {
       _id : $scope.user._id,
       creator_id : $scope.user.creator_id,
       date: $scope.user.date,
       comments:  $scope.user.comments,
      };
    
     $http.post('/user/postUpdateWorkLog', data)
        .then(function(response) { 
      }).
    catch(function(response) {
      $scope.user = {};
      $scope.error = data;
    })      
       
    }
     
     $scope.refreshPage = function($route) {
      $window.location.reload();
     }
    
     $scope.closePage = function() { 
       $window.location.assign('/');
        //$route.reload();      
      }   
      
     $scope.nextRecord = function() {    
      if (! $scope.array[recordCount])
       {
         recordCount = 0;
        }     
       $scope.user =  $scope.array[recordCount] ;
       var filename = "/images/" + $scope.user.timeStamp + ".jpg";
       var img = filename;
       $scope.image = img;               
       recordCount = recordCount + 1;
      }     

  }]); 