/* Angular Upload module is used to upload images from the client's computer browser to the web server. The $http 
service is a core AngularJS service that facilitates communication with the web server via the browser's HttpRequest objects. */


angular.module('mainApp', ['ngFileUpload']).
  controller('mainController', ['Upload', '$scope', '$http', '$window',
 
                              function(Upload, $scope,  $http, $window) {     
        
    var vm = this;
    vm.submit = function(){ //function to call on form submit
        if (vm.upload_form.file.$valid && vm.file) { //check if from is valid
            vm.upload(vm.file); //call upload function
        }
    }
    
vm.upload = function (file) {
        Upload.upload({
            url: 'http://localhost:3000/upload', //webAPI exposed to upload the file
            data:{file:file} //pass file as data, should be user ng-model
        }).then(function (resp) { //upload function returns a promise
            if(resp.data.error_code === 0){ //validate success
              //  $window.alert('Success ' + resp.config.data.file.name + 'uploaded. Response: ');
            } else {
                $window.alert('an error occured');
            }
        }, function (resp) { //catch error
            console.log('Error status: ' + resp.status);
            $window.alert('Error status: ' + resp.status);
        }, function (evt) { 
            console.log(evt);
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            vm.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
            
        });
    };

     var date = new Date();
     $scope.date = date.toUTCString();
     $scope.date = $scope.date.slice(5,17);
     
    $scope.addDocToLocalLinkDB = function() {       
     var data = {
       date: $scope.date,
       comments: $scope.comments
      };
    
     $http.post('/', data)
        .then(function(response) {
        $scope.summary = "";   
     }).
    catch(function(response) {
      $scope.user = {};
      $scope.error = data;
    }); 
     
     var date = new Date();
     $scope.date = date.toUTCString();
     $scope.date = $scope.date.slice(5,17);
     $scope.comments ="";
     vm.progress = ''; 
    } 

    

  }]).controller('MyCtrl',[ '$scope', '$http', '$window',
function( $scope,  $http, $window){

    $scope.openSearch = function () { 
      
        var data = {
       date: $scope.date,
       comments: $scope.comments
      };
    
     $http.post('/user/search', data)
        .then(function(response) {
        $scope.summary = "";   
     }).
     catch(function(response) {
      $scope.user = {};
      $scope.error = data;
     }); 
       
      $window.location.assign('/search');      
     }


  $scope.logout = function () { 
    $window.location.assign('/logout'); 
  }



}]);
