// Event listener for job accepting button
$(document).on( "click", "[class*='btn-primary'][id='jbautoacpt']", function(event) {
    var form = $(this).parent();
    $.ajax({
    type: 'POST',
    url: '/acceptjob',
    dataType: 'json',
    data: form.serialize()
    }).done(function(data) {
        if(data.accepted == 1){
            $(event.target).text('Job Accepted!');
            $(event.target).parent().find("[class*='btn-primary']").removeClass().addClass('btn btn-secondary btn-job');            
        } else {
            $(event.target).text("Application Error!");
            $(event.target).removeClass().addClass('btn btn-secondary btn-job');
        }
    }).fail(function(data) {        
        $(event.target).text("Application Error!");
        $(event.target).removeClass().addClass('btn btn-secondary btn-job');    
    });
});