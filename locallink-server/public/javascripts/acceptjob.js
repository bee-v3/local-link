// Event listener for job accepting button
$(document).on( "click", "[class*='btn-primary'][id='jbaccept']", function(event) {
    var form = $(this).parent();
    $.ajax({
    type: 'POST',
    url: '/acceptjob',
    dataType: 'json',
    data: form.serialize()
    }).done(function(data) {
        if(data.accepted == 1){
            $(event.target).text('Application Accepted!');
            $(event.target).parents().each(function(){                
                $("[class*='btn-primary'][id='jbaccept']").removeClass().addClass('btn btn-secondary btn-job');
            });      
            $(event.target).removeClass().addClass('btn btn-success btn-job');            
        } else {
            $(event.target).text("Application Error!");
            $(event.target).removeClass().addClass('btn btn-secondary btn-job');
        }
    }).fail(function(data) {        
        $(event.target).text("Application Error!");
        $(event.target).removeClass().addClass('btn btn-secondary btn-job');    
    });
});
function showPayment() {
    var x = document.getElementById("payment");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
};