/**
* Dynamically styles the dropdowns
**/
function categoryDropDown() {
  var selections, i, j, selElmnt, newOption, optionList, optionItem;
  // Grab all elements with custom-select class
  selections = document.getElementsByClassName("custom-select");

  for (i = 0; i < selections.length; i++) {
    // Get next category selection
    selElmnt = selections[i].getElementsByTagName("select")[0];

    // Create new divs to act as the selected item
    newOption = document.createElement("DIV");
    newOption.setAttribute("class", "select-selected");
    newOption.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    selections[i].appendChild(newOption);
    
    optionList = document.createElement("DIV");
    optionList.setAttribute("class", "select-items select-hide");

    for (j = 1; j < selElmnt.length; j++) {

      // Create option item divs
      optionItem = document.createElement("DIV");
      optionItem.innerHTML = selElmnt.options[j].innerHTML;
      optionItem.addEventListener("click", function(e) {
         
          // Update selected item
          var oldOption, i, k, options, previous;
          options = this.parentNode.parentNode.getElementsByTagName("select")[0];
          previous = this.parentNode.previousSibling;
          for (i = 0; i < options.length; i++) {
            if (options.options[i].innerHTML == this.innerHTML) {
              options.selectedIndex = i;
              previous.innerHTML = this.innerHTML;
              oldOption = this.parentNode.getElementsByClassName("same-as-selected");
              for (k = 0; k < oldOption.length; k++) {
                oldOption[k].removeAttribute("class");
              }
              this.setAttribute("class", "same-as-selected");
              break;
            }
          }
          previous.click();
      });
      optionList.appendChild(optionItem);
    }
    selections[i].appendChild(optionList);
    newOption.addEventListener("click", function(e) {
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
      });
  }

  /**
  * Function to close all select boxes in the document, except the current select box
  **/
  function closeAllSelect(elmnt) {
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
      if (elmnt == y[i]) {
        arrNo.push(i)
      } else {
        y[i].classList.remove("select-arrow-active");
      }
    }
    for (i = 0; i < x.length; i++) {
      if (arrNo.indexOf(i)) {
        x[i].classList.add("select-hide");
      }
    }
  }

  //If the user clicks anywhere outside the select box, then close all select boxes
  document.addEventListener("click", closeAllSelect);
}



/**
*	Grabs jobs within a certain category from the database.
*
*	Note: Other search criteria such as mile radius, dollar amount, etc. to be
*	implemented later.
*
*	@param category - The job category the user wanted to search.
**/

function grabJobListings(category) {
	// To be implemented later
	// Query the database and return all jobs of a certain category

	// Placeholder job list, only holds job title
	var jobListings = ["Fence painting", "Lawn mowing", "Lawn mowing", "Weed pulling"];

	appendJobListings(jobListings);
}

/**
*	Appends job listings to the job-listings div.
*
*	@param jobListings - All the job listings grabbed from the database.
**/
function appendJobListings(jobListings) {

  // Placeholder job list to test dropdowns
  var jobs = ["Yard work", "General repairs", "Pet care", "Home maintenance", "Caregiving"];

  var distance = document.getElementById("distanceSelect").value;
  var job = document.getElementById("jobSelect").value;

  $("#picked").html("Showing results for " + jobs[job - 1] + " within " + distance + " mile(s)");

	$.each(jobListings, function(i) {

		$("#job-listings").append('<div class="job-listing"><div class="job-details">' +
                          '<div class="job-title">' +
                            jobListings[i] + 
                          '</div>' +
                          '<div class="poster-info">' +
                            '<div class="posted-by">Posted by John S.<span id="verified">Verified User</span></div>' +
                            '<div class="poster-rating">' +
                              '<fieldset class="rating">' +
                                  '<input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="5-stars"></label>' +
                                  '<input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="4.5-stars"></label>' +
                                  '<input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="4-stars"></label>' +
                                  '<input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="3.5-stars"></label>' +
                                  '<input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="3-stars"></label>' +
                                  '<input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="2.5-stars"></label>' +
                                  '<input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="2-stars"></label>' +
                                  '<input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="1.5-stars"></label>' +
                                  '<input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="1-star"></label>' +
                                  '<input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title=".5-stars"></label>' +
                              '</fieldset>' +
                            '</div>' +
                          '</div>' +
                        '</div>' +

                        '<div class="miles-away">8 miles away</div>' +

                        '<div class="job-pay">$00.00</div>' +

                        '<div class="job-buttons"><button id="view-button">View Job</button><button id="save-button">Save Job</button></div></div>')
	})
}
