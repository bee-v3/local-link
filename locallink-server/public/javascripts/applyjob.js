// Event listener for job accepting button
$(document).on( "click", "[class*='btn-primary'][id='jbapply']", function(event) {
    var form = $(this).parent();
    $.ajax({
    type: 'POST',
    url: '/applyjob',
    dataType: 'json',
    data: form.serialize()
    }).done(function(data) {
        if(data.accepted == 1){
            $(event.target).text('Application Sent!');
            $(event.target).removeClass().addClass('btn btn-success btn-job');
        } else {
            $(event.target).text("Application Error!");
            $(event.target).removeClass().addClass('btn btn-secondary btn-job');
        }
    }).fail(function(data) {        
        $(event.target).text("Application Error!");
        $(event.target).removeClass().addClass('btn btn-secondary btn-job');    
    });
});