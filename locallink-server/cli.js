("use strict");
const inquirer = require("inquirer");
const pool = require("./lib/postgres").pgPool;
const generate = require("./accGen");
var readline = require("readline");

async function questions(job_id, worker_id) {
  var questions = [
    {
      type: "list",
      name: "main",
      message: "What do you want to do?",
      choices: [
        {
          value: "basic",
          name: "Create basic accounts with data for tables: users, location"
        },
        {
          value: "post",
          name:
            "Create job post accounts with data for tables: users, location, job_post"
        },
        {
          value: "worker",
          name:
            "Create worker accounts with data for tables: users, profile, location, worker"
        },
        {
          value: "link",
          name: "Connect job posts to random workers"
        },
        // {
        //   value: "delete_user",
        //   name: "Remove accounts by username/user_id"
        // },
        {
          value: "delete_table",
          name: "Remove data from select tables"
        }
      ],
      when: function(answers) {
        return !("main" in answers);
      }
    },
    {
      type: "input",
      name: "linkNum",
      message: `There are currently ${worker_id.rowCount} workers, and ${job_id.rowCount} job posts without assigned workers. \nHow many jobs would you like to assign a worker?`,
      validate: function(value) {
        var valid = !isNaN(parseFloat(value));
        if (value > job_id.rowCount) {
          return "Please enter a number less than or equal to the number of jobs";
        }
        if (worker_id.rowCount == 0) {
          return "Not enough workers, enter CTRL + C to quit";
        }
        return valid || "Please enter a number";
      },
      when: function(answers) {
        return ["link"].includes(answers.main);
      }
    },
    {
      type: "input",
      name: "num",
      message: "How many accounts would you like to create?",
      validate: function(value) {
        var valid = !isNaN(parseFloat(value));
        return valid || "Please enter a number";
      },
      when: function(answers) {
        return ["login", "basic", "post", "worker"].includes(answers.main);
      }
    },
    // {
    //   type: "input",
    //   name: "user_id",
    //   message: "Enter the user name of the account you would like to remove",
    //   when: function (answers) {
    //     return answers.main == "delete_user";
    //   }
    // },
    {
      type: "checkbox",
      name: "tables",
      message: "Select tables to delete.",
      choices: [
        { name: "users" },
        { name: "location" },
        { name: "job_post" },
        { name: "worker" }
      ],
      when: function(answers) {
        return answers.main == "delete_table";
      }
    },
    {
      type: "confirm",
      name: "confirm",
      message: `Confirm action`
    }
  ];
  return questions;
}

var insert_users = `
INSERT INTO users (username, password, fname, lname, email, phone, about_msg, is_worker, is_privet, post_history, pic_id)
VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`;

var insert_location = `
INSERT INTO location (user_id, address, city, state, zip, coordinates)
VALUES($1, $2, $3, $4, $5, ST_GeomFromText($6, 4326))`;

var insert_post = `
INSERT INTO job_post (job_id, poster_id, worker_id, post_date, completed_date, job_desc, price, chat_ids, is_privet, view_rad, skills, jobCategories, title)
VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)`;

var insert_worker = `
INSERT INTO worker (user_id, skills, payrate, job_hist, workRadius, jobCategories, avgRating)
VALUES($1, $2, $3, $4, $5, $6, $7)`;

var prune = `SELECT user_id FROM location WHERE address ~* '^[a-z]';`;

(async function() {
  const job_id = await pool.query(
    "select job_id from job_post where worker_id is null"
  );
  const worker_id = await pool.query("select user_id from worker");

  const answers = await inquirer.prompt(await questions(job_id, worker_id));
  // console.log(JSON.stringify(answers, null, "  "));
  if (answers.confirm == true) {
    if (["basic", "post", "worker"].includes(answers.main)) {
      for (i = 0; i < answers.num; i++) {
        let account;
        if (answers.main == "basic") {
          account = await generate.basic_account();
          try {
            await pool.query(insert_users, Object.values(account.users));
            await pool.query(insert_location, Object.values(account.location));
          } catch (err) {
            console.log(err);
          }
        }
        if (answers.main == "post") {
          account = await generate.account_with_post();
          try {
            await pool.query(insert_users, Object.values(account.users));
            await pool.query(insert_location, Object.values(account.location));
            await pool.query(insert_post, Object.values(account.job_post));
          } catch (err) {
            console.log(err);
          }
        }
        if (answers.main == "worker") {
          account = await generate.worker_account();
          try {
            await pool.query(insert_users, Object.values(account.users));
            await pool.query(insert_location, Object.values(account.location));
            await pool.query(insert_worker, Object.values(account.worker));
          } catch (err) {
            console.log(err);
          }
        }
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`${((i + 1) / answers.num) * 100}%`);
      }
      pool.end();
      console.log(
        `\nSucessfully generated and inserted ${answers.num} accounts\nAll accounts have the password: silver`
      );
    }

    if (answers.main == "link") {
      for (i = 0; i < answers.linkNum; i++) {
        let worker =
          worker_id.rows[Math.floor(Math.random() * worker_id.rows.length)]
            .user_id;
        try {
          await pool.query(
            `UPDATE job_post SET worker_id='${worker}' WHERE job_id = '${job_id.rows[i].job_id}'`
          );
          await pool.query(
            `UPDATE worker SET job_hist = array_append(job_hist,'${job_id.rows[i].job_id}') WHERE user_id='${worker}'`
          );
        } catch (err) {
          console.log(err);
        }
      }
      pool.end();
    }

    // if (answers.main == 'delete_user') {
    //     try {
    //       await pool.query(`DELETE FROM ${table}`);
    //     } catch (err) {
    //       console.log(err);
    //     }
    //   pool.end();
    // }

    if (answers.main == "delete_table") {
      for (table of answers.tables) {
        try {
          await pool.query(`DELETE FROM ${table}`);
        } catch (err) {
          console.log(err);
        }
      }
      pool.end();
    }
  }
})();
