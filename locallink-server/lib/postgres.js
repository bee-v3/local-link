/*  This file stores the PostgreSQL database location 
    and connection pool. */

const { Pool } = require('pg')

module.exports.pgPool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_USER_PASSWORD,
  port: process.env.DB_PORT,
  max: 20 // Maximum number of client connections to database
});