/*  This file stores strings of queries to the
    PostgreSQL database . */

module.exports ={
createReview : "INSERT INTO review(review_id, user_id, reviewed_by, rev_desc, rating, rev_date, job_ref) VALUES($1, $2, $3, $4, $5, $6, $7)",
selectReview : "SELECT rating, rev_desc, reviewed_by FROM review WHERE user_id = $1",
findRatings : "SELECT rating FROM review WHERE user_id = $1",
workerCheck : "SELECT * FROM worker WHERE user_id = $1",
updateAvgrating : "UPDATE worker SET avgrating = $1::numeric WHERE user_id = $2",
registeruser : "INSERT INTO users(username, password, fname, lname, email, phone, about_msg) VALUES($1, $2, $3, $4, $5, $6, $7)",
createworker : "INSERT INTO worker(user_id) VALUES($1)",
insertLoc: "INSERT INTO location (user_id, address, city, state, zip, coordinates) VALUES($1, $2, $3, $4, $5, ST_GeomFromText($6, 4326))",
getLoc: "SELECT ST_AsText(coordinates) FROM location WHERE user_id = $1",
loc:"SELECT * FROM location WHERE user_id = $1",
getworkerinfo: "SELECT * FROM worker WHERE user_id = $1",
updateusername : "UPDATE users SET username = $1 WHERE username IS $2",
updateemail : "UPDATE users SET email = $2 WHERE username IS $1",
updateuserinfo : "UPDATE users SET email = $2, phone = $3, about_msg = $4 WHERE username = $1",
updateuserlocation: "UPDATE location SET address = $2, city = $3, state = $4, zip = $5, coordinates = ST_GeomFromText($6, 4326) WHERE user_id = $1",
changepassword : "UPDATE users SET password = $2 WHERE username IS $1",
loginuser : "SELECT * FROM users WHERE username = $1",
searchemail : "SELECT username FROM users WHERE email = $1",
postjob : "INSERT INTO job_post(job_id, poster_id, post_date, job_desc, price, view_rad, title, skills, jobCategories, post_time, needs_compl_photo) VALUES($1,$2,$3,$4,$5,$6,$7,ARRAY[$8],ARRAY[$9], NOW(), $10)",
postworker: "UPDATE worker SET skills=$2, payrate=$3, workRadius=$4, jobCategories=$5, avgrating=$6::numeric WHERE user_id=$1",
getJob : "SELECT * FROM job_post WHERE job_id = $1",
getProfile: "SELECT fname,lname,about_msg,post_history,pic_id FROM users WHERE username = $1",
acceptjob_step1:  "UPDATE job_post SET worker_id = $1 WHERE job_id = $2",
acceptjob_step2:  "UPDATE worker SET job_hist = array_append(job_hist, $2) WHERE user_id = $1",
acceptjob_step3:  "UPDATE job_post SET applied_worker = NULL WHERE job_id = $1",
complreq: "UPDATE job_post SET compl_message = $1, needs_completion = TRUE, reject_message = NULL WHERE job_id =$2",
complresp1: "UPDATE job_post SET reject_message = $1, completed_date = NOW() WHERE job_id =$2",
complresp2: "UPDATE job_post SET reject_message = $1, compl_message = NULL, needs_completion = FALSE WHERE job_id =$2",
applyjob: "UPDATE job_post SET applied_worker = array_append(applied_worker, $1) WHERE job_id = $2",
findWorkers: `
SELECT * 
FROM (
    SELECT user_id
    FROM (
        SELECT coordinates
        FROM location
        WHERE user_id = $1
    ) AS search_loc, (
        SELECT worker.user_id, coordinates
        FROM worker, location
        WHERE worker.user_id = location.user_id
    ) as workers
    WHERE ST_DWithin(search_loc.coordinates, workers.coordinates, 16093, false)
)AS selected, worker
WHERE selected.user_id = worker.user_id
AND avgrating >= '3' 
AND payrate <= $2::smallint 
AND skills @> $3::varchar[]
ORDER BY avgrating DESC LIMIT 3`,
findRecentJob: "SELECT skills, price FROM job_post WHERE poster_id = $1 ORDER BY post_time DESC LIMIT 1",
findWorkersNoJobs: "SELECT * FROM worker WHERE avgrating IS NOT NULL AND user_id <> $1 ORDER by avgrating DESC LIMIT 3",
query10 : "SELECT * from job_post WHERE worker_id IS NULL fetch first 10 rows only",
deleteJob : "DELETE FROM job_post WHERE job_id = $1::varchar",
clearPostHistory : "UPDATE users SET post_history = '{}' WHERE $1 = username",
allMyJobs : "Select * from job_post WHERE poster_id = $1",
allMyActiveJobs : "SELECT * from job_post WHERE poster_id = $1 AND completed_date IS NULL",
allMyActiveWorking : "SELECT * from job_post WHERE worker_id = $1 AND completed_date IS NULL",
allMyPastPosted : "SELECT * from job_post WHERE poster_id = $1 AND completed_date IS NOT NULL",
allMyPastWorked : "SELECT * from job_post WHERE worker_id = $1 AND completed_date IS NOT NULL",
updatePostHistory : "UPDATE users SET post_history = array_append(post_history, $2) WHERE username = $1",
jobSearchDefault: `
SELECT *
FROM (
    SELECT job_id
    FROM (
        SELECT coordinates
        FROM location
        WHERE user_id = $1
    )AS search_loc, (
        SELECT job_id, coordinates, worker_id
        FROM job_post, location
        WHERE job_post.worker_id is NULL
        AND job_post.poster_id = location.user_id
    )AS jobs
    WHERE ST_DWithin(search_loc.coordinates, jobs.coordinates, 16093, false)
    )AS selected, job_post
    WHERE selected.job_id = job_post.job_id`,
jobSearchSpec:`SELECT *
FROM (
    SELECT job_id
    FROM (
        SELECT coordinates
        FROM location
        WHERE user_id = $1
    )AS search_loc, (
        SELECT job_id, coordinates, worker_id
        FROM job_post, location
        WHERE job_post.worker_id is NULL
        AND job_post.poster_id = location.user_id
        AND $2 && job_post.jobCategories
    )AS jobs
    WHERE ST_DWithin(search_loc.coordinates, jobs.coordinates, $3, false)
    )AS selected, job_post
    WHERE selected.job_id = job_post.job_id`,
workerSearch:`
SELECT * 
FROM (
    SELECT user_id
    FROM (
        SELECT coordinates
        FROM location
        WHERE user_id = $1
    ) AS search_loc, (
        SELECT worker.user_id, coordinates
        FROM worker, location
        WHERE worker.user_id = location.user_id
    ) as workers
    WHERE ST_DWithin(search_loc.coordinates, workers.coordinates, $3, false)
)AS selected, worker
WHERE selected.user_id = worker.user_id
AND payrate <= $4::smallint 
AND $2 && worker.skills
`
}