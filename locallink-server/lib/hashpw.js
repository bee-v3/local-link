var crypto = require("crypto"); /* used for password hashing */

/* used for password hashing */
module.exports = function hashPW(pwd) {
    return crypto
      .createHash("sha256")
      .update(pwd)
      .digest("base64")
      .toString();
  }