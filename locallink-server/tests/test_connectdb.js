#!/usr/bin/env node

/**
 * Module dependencies.
 */
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var pgSession = require('connect-pg-simple')(session);
var pool = require('../lib/postgres');

/*
var app = express();

app.use(session({
  store: new pgSession({
    pool: pool.pgPool,
    tableName: 'session',
    //ttl: 30 // temporary for testing
  }),
  secret: 'SECRET',
  cookie: {maxAge: 30 * 24 * 60 * 60 * 1000},
  saveUninitialized: false,
  resave: false
}));*/



var write1 = "INSERT INTO users(name, email) VALUES($1, $2)";
var read1 = "SELECT * FROM users LIMIT 100";
var cleartable = "DELETE FROM users";
var nameemail1 = ['test','test1@gmail.com'];
var nameemail2 = ['test2','test2@gmail.com'];
var nameemailshouldfail = ['test3','test1@gmail.com'];

// Adds 'test','test1@gmail.com' as a row in database
pool.pgPool.query(write1, nameemail1, (err, result) => {
  if (err) {
    return console.error('Error executing query', err.stack)
  }
  console.log(result);
})

// Adds 'test2','test2@gmail.com'
pool.pgPool.query(write1, nameemail2, (err, result) => {
  if (err) {
    return console.error('Error executing query', err.stack)
  }
  console.log(result);
})

// Query should fail due to the email field being unique in the database
pool.pgPool.query(write1, nameemailshouldfail, (err, result) => {
  if (err) {
    return console.error('Error executing query', err.stack)
  }
  console.log(result);
})

// Outputs the two rows and their values
pool.pgPool.query(read1, (err, result) => {
  if (err) {
    return console.error('Error executing query', err.stack)
  }
  console.log(result);
})

// Clears the rows from the table for consistent results
pool.pgPool.query(cleartable, (err, result) => {
  if (err) {
    return console.error('Error executing query', err.stack)
  }
  console.log(result);
  pool.pgPool.end(); // close connection to the database
  process.exit();  // end program
})