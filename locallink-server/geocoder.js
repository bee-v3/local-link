var NodeGeocoder = require("node-geocoder");

var options = {
  provider: "google",
  httpAdapter: "https",
  apiKey: "AIzaSyBrSsf2VRjUFCcWqaY5_WSaLmZwACmxH5U",
  formatter: null
};

var geocoder = NodeGeocoder(options);

module.exports = geocoder;

// geocoder.reverse({lat:45.767, lon:4.833}, function(err, res) {
//   console.log(res);
// });

// geocoder.geocode({address: '29 champs elysée', country: 'France', zipcode: '75008'}, function(err, res) {
//   console.log(res);
// });

/* with promise */

// geocoder.geocode({ address: "123 main st", country: "US", zipcode: "22902" })
//   .then(function(res) {
//     console.log(res);
//   })
//   .catch(function(err) {
//     console.log(err);
//   });

// geocoder.reverse({ lat: latitude, lon: longitude })
//     .then(function (res) {
//       console.log(res);
//     })
//     .catch(function (err) {
//       console.log(err);
//     });
