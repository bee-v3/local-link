/* Routes for '/login' */
var express = require('express');
var router = express.Router();

const users = require("../controllers/login_controller");

  router.get("/login", async (req, res) => {
    if (req.session.username) {
      res.redirect("/");
    }    
    res.render("login", { msg: req.session.msg });
  });
  
  router.post("/login", users.login);

module.exports = router;