var express = require("express");
var router = express.Router();
var users = require("../controllers/selectViewReview_controller");
const PostService = require("../controllers/post_controller");

router.get("/viewReview", async (req, res) => {
if(req.query.username){
  var reviews = await users.viewReview(req.query.username);
  var jsonobj = {};
  jsonobj.reviews = reviews.rows;
  jsonobj.username = req.query.username;
  res.render('viewReview', {arr: jsonobj}); 
} else{
  res.redirect('/selectReview');
}}) ;

router.get("/selectReview", function(req, res) {
   /*if (!req.session.username) {
    res.redirect("/");
  }*/
  res.render("selectReview", { msg: req.session.msg });
});

router.post("/selectReview", users.selectReview);

module.exports = router;
