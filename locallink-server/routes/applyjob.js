/* Routes for '/login' */
var express = require('express');
var router = express.Router();

const users = require("../controllers/jobapply_controller");

router.post("/applyjob", async (req, res) => {
  var response = await users.jobapply(req.session.username, req.body.job_id);
  res.json(response);
});

module.exports = router;