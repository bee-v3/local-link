module.exports = function(io){
	var express = require('express');
	var router = express.Router();

	const chatInit = require("../controllers/chat_controller")(io);

	router.get("/initializeChat",async (req,res)=>{
		res.render("initializeChat");
	});

	router.post("/initializeChat", async(req,res)=>{
		let toUser = req.body.username;
		res.redirect("/chat/"+toUser);
	});

	router.get("/chat/:toUser",chatInit.chat);

	return router;
}