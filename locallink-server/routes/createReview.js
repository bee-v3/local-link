var express = require("express");
var router = express.Router();
var users = require("../controllers/createReview_controller");
const PostService = require("../controllers/post_controller");


router.get("/createReview", function(req, res) {
   if (!req.session.username) {
    res.redirect("/");
  }
  res.render("createReview", { msg: req.session.msg });
});


router.post("/createReview", users.createReview);


module.exports = router;
