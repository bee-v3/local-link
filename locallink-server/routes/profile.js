/* Routes for '/login' */
var express = require('express');
var router = express.Router();

const users = require("../controllers/profile_controller");

router.get("/profile", async (req, res) => {
  if (typeof req.session.username == 'undefined') {
    res.redirect("/");
  }
  var myjobs = await users.getjobs(req);
  console.log(req.session.username);
  res.render("profile", { session: req.session, jobs: myjobs });
});

router.post("/profile", function (req, res) {
  users.deleteMyJob(req, res);
});

module.exports = router;