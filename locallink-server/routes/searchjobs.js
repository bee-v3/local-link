module.exports = function(io){
  var express = require('express');
  var router = express.Router();

  var PostService = require("../controllers/post_controller");
  const chatInit = require("../controllers/chat_controller")(io);

  router.get("/searchjobs", async (req, res, next) => {
    //console.log(req.query)
    if (!req.session.username) {
      if(req.query.hasOwnProperty('category') && req.query.hasOwnProperty('distance')){
        try {
          return res.redirect("login");
        }catch (err) {
          return next(err);
        }
      }else{
        try {
          const posts = await PostService.get10();
          return res.render("searchjobs", { posts, user: req.session.username});
        }catch (err) {
          return next(err);
        }
      }
    } else {
      if(req.query.hasOwnProperty('category') && req.query.hasOwnProperty('distance')){
        try {
          const posts = await PostService.jobSearchSpec(req.session.username, req.query.category, req.query.distance);
          return res.render("searchjobs", { posts, user: req.session.username });
        }catch (err) {
          return next(err);
        }
        
      }else{
        try {
          const posts = await PostService.jobSearchDefault(req.session.username);
          return res.render("searchjobs", { posts, user: req.session.username });
        }catch (err) {
          return next(err);
        }
      }
    }
  });

  router.get("/job/:job_id", async (req, res, next) => {
    try {
      var post = await PostService.getJob(req.params.job_id);
      post.location = await PostService.getLocation(post.poster_id);
      return res.render("job", { post, user: req.session.username });
    } catch (err) {
      return next(err);
    }
  });

  router.post("/job/:chat_id", async(req,res)=>{
    let toUser = req.body.username;
    res.redirect("/chat/"+toUser);
  });

  router.get("/chat/:toUser",chatInit.chat);

  return router;
}