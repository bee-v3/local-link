/* Routes for '/login' */
var express = require('express');
var router = express.Router();


  router.get("/logout", (req, res) => {
    req.session.destroy(function() {
      res.redirect("/");
    });
  });
  
  module.exports = router;