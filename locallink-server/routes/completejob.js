var express = require("express");
var router = express.Router();
var users = require("../controllers/completejob_controller");
var PostService = require("../controllers/post_controller");

router.get("/completejob/", async (req, res, next) => {
  if(req.query.job_id && req.session.username){
    const post = await PostService.getJob(req.query.job_id);
    if(post.worker_id == req.session.username){
      res.render("completejob", { post : post, user: req.session.username });
    } else{
      res.redirect("/");
    }
  } else{
    res.redirect("/");
  }
});

router.post("/completejob/", async(req,res)=>{
  if(req.query.job_id && req.session.username){
    var response = await users.completejob(req.body.message,req.body.job_id);
    if(response.accepted == 0){
      console.log(response.msg);
      res.redirect("/");
    }
    else{
      console.log(response.msg);
      res.redirect("myjobs");
    }
  } else{
    res.redirect("/");
  }
});

router.post("/completejob_response", async(req,res)=>{
  if(req.body.jobid && req.session.username){
    var response = await users.completejob_response(req.body.message, req.body.compl_response, req.body.jobid);
    if(response.accepted == 0){
      console.log(response.msg);
      res.redirect("/");
    }
    else{
      console.log(response.msg);
      res.redirect("myjobs");
    }
  } else{
    res.redirect("/");
  }
});

module.exports = router;