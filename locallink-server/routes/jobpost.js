/* Routes for '/login' */
var express = require('express');
var router = express.Router();

const users = require("../controllers/jobpost_controller");

router.get("/jobpost", function(req, res) {
  if (!req.session.username) {
    res.redirect("/");
  }
  res.render("jobpost", { msg: req.session.msg });
});

router.post("/jobpost", users.jobpost);

module.exports = router;