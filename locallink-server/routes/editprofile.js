/* Routes for '/login' */
var express = require('express');
var router = express.Router();

const users = require("../controllers/editprofile_controller");


router.get("/editprofile", async (req, res) => {
  if (typeof req.session.username == 'undefined') {
    res.redirect("/");
  }
  res.render("editprofile", { userinfo: req.session });
});

router.post("/editprofile", users.editprofile);

module.exports = router;