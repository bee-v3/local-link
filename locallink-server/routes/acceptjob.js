/* Routes for '/login' */
var express = require('express');
var router = express.Router();

const users = require("../controllers/jobaccept_controller");

router.post("/acceptjob", async (req, res) => {
  var response = await users.jobaccept(req.body.worker_id, req.body.job_id);
  //if(response.accepted == 1){req.session.job_hist.push(req.body.job_id);}
  res.json(response);
});

module.exports = router;