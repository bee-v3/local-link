var express = require('express');
var router = express.Router();

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", {session : req.session});
});

/*
router.get('/search', function(req, res){
       
    if (req.session.user) {
        res.render('search', {msg:req.session.msg});
      
    } else {
      req.session.msg = 'Access denied!';
      res.redirect('/login');
    }
  });*/

router.get("/postsuccess", function(req, res) {
  res.render("postsuccess", { msg: req.session.msg });
});

//router.get('/user/search', users.searchLocalLink_db);

/* Multer adds a body object and a file or files object to the request object. The body object contains the
 values of the text fields of the form, the file or files object contains the files uploaded via the form */
//router.post('/upload', users.uploader);

//router.post('/', users.addDocToLocalLink);

module.exports = router;