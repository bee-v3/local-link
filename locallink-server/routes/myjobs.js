var express = require('express');
var router = express.Router();
var mj = require("../controllers/myjobs_controller");


router.get("/myjobs", async (req, res) => {
  if (!req.session.username) {
    res.redirect("/");
  } else{
  var activeJobs = await mj.myActiveJobs(req);
  var activeWorking = await mj.myActiveWorking(req);
  var myposted = await mj.myPastPosted(req);
  var myworked = await mj.myPastWorked(req);
  res.render("myjobs", { session: req.session, active: activeJobs, working: activeWorking, posted: myposted, worked: myworked });
}});

router.post("/myjobs", function (req, res) {
  mj.deleteMyJob(req, res);
});

module.exports = router;