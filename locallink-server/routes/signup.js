/* Routes for '/login' */
var express = require('express');
var router = express.Router();

const users = require("../controllers/signup_controller");

router.get("/signup", function(req, res) {
  if (req.session.username) {
    res.redirect("/");
  }
  res.render("signup", { msg: req.session.msg });
});
  
router.post("/signup", users.signup);

module.exports = router;