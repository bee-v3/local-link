/* Routes for '/login' */
var express = require('express');
var router = express.Router();

const users = require("../controllers/workerpost_controller");

router.get("/workerpost", function(req, res) {
  if (!req.session.username) {
    res.redirect("/");
  }
  res.render("workerpost", { msg: req.session.msg });
});

router.post("/workerpost", users.workerpost);

module.exports = router;