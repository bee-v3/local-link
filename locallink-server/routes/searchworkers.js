module.exports = function(io){
	var express = require('express');
	var router = express.Router();

	var fetchWorkers = require("../controllers/searchworkers_controller");
	const chatInit = require("../controllers/chat_controller")(io);

	/* GET search workers page*/
	router.get("/searchworkers", async (req, res, next) => {
		if(!req.session.username){//if a user is not logged in 
			if(req.query.hasOwnProperty('category') && req.query.hasOwnProperty('distance')){
			// if a user is not logged in and does a specific search (either category or distance or price), redirect to the login page
				try {
				  return res.redirect("login");
			}catch (err) {
				  return next(err);
			}
			}else{
			// if a user is not logged in and goes to /searchworkers
				try {
					var matchedWorkers = await fetchWorkers.matchWorkers(req);
					const allWorkers = await fetchWorkers.getAll();
					res.render("searchworkers", {session : req.session, worker : allWorkers, matched : matchedWorkers});
				}catch (err) {
					return next(err);
				}
			}
		} 
		else {// if a user is logged in
			if(req.query.hasOwnProperty('category') && req.query.hasOwnProperty('distance')){
			// if a user is logged in and does a specific search (either category or distance or price), matched workers are not showed and searched for workers are
				try{
					var allWorkers = await fetchWorkers.workerSearch(req.session.username, req.query.category, req.query.distance, req.query.price);
					res.render("searchworkers", {session : req.session, worker : allWorkers});
				} catch(err) {
					return next(err);
				}
			}else{
				// if a user is logged in and goes to /searchworkers, matched workers (based on last job post) and all workers in area are shown
				try{
					var matchedWorkers = await fetchWorkers.matchWorkers(req);
					var allWorkers = await fetchWorkers.workerSearch(req.session.username, 'All', 16093, 150);
					res.render("searchworkers", {session : req.session, worker : allWorkers, matched : matchedWorkers});
				} catch(err) {
					return next(err);
				}
			}
		}
	});

	router.post("/searchworkers", async(req,res)=>{
		let toUser = req.body.username;
		res.redirect("/chat/"+toUser);
	});

	router.get("/chat/:toUser",chatInit.chat);

	return router;
}