module.exports = {
  users: function(
    username,
    password,
    fname,
    lname,
    email,
    phone,
    about_msg,
    is_worker,
    is_privet,
    post_history,
    pic_id
  ) {
    this.username = username;
    this.password = password;
    this.fname = fname;
    this.lname = lname;
    this.email = email;
    this.phone = phone;
    this.about_msg = about_msg;
    this.is_worker = is_worker;
    this.is_privet = is_privet;
    this.post_history = post_history;
    this.pic_id = pic_id;
  },
  location: function(user_id, address, city, state, zip, coordinates) {
    this.user_id = user_id;
    this.address = address;
    this.city = city;
    this.state = state;
    this.zip = zip;
    this.coordinates = coordinates;
  },
  job_post: function(
    job_id,
    poster_id,
    worker_id,
    post_date,
    completed_date,
    job_desc,
    price,
    chat_ids,
    is_privet,
    view_rad,
    skills,
    jobCategories,
    title
  ) {
    this.job_id = job_id;
    this.poster_id = poster_id;
    this.worker_id = worker_id;
    this.post_date = post_date;
    this.completed_date = completed_date;
    this.job_desc = job_desc;
    this.price = price;
    this.chat_ids = chat_ids;
    this.is_privet = is_privet;
    this.view_rad = view_rad;
    this.skills = skills;
    this.jobCategories = jobCategories;
    this.title = title;
  },
  worker: function(
    user_id,
    skills,
    payrate,
    job_hist,
    work_rad,
    jobCategories,
    avgRating
  ) {
    this.user_id = user_id;
    this.skills = skills;
    this.payrate = payrate;
    this.job_hist = job_hist;
    this.work_rad = work_rad;
    this.jobCategories = jobCategories;
    this.avgRating = avgRating;
  }
};
