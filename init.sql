CREATE EXTENSION postgis;

CREATE TABLE "session" (
  "sid" varchar NOT NULL COLLATE "default",
	"sess" json NOT NULL,
	"expire" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE);

ALTER TABLE "session" ADD CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE;

CREATE INDEX "IDX_session_expire" ON "session" ("expire");

CREATE TABLE users (
   username VARCHAR (64) PRIMARY KEY,
   password VARCHAR (64) NOT NULL,
   fname VARCHAR (30),
   lname VARCHAR (30),
   email VARCHAR (100),
   phone VARCHAR (12),
   about_msg VARCHAR,
   is_worker BOOLEAN,
   is_privet BOOLEAN,
   matched_workers VARCHAR [],
   post_history VARCHAR [],
   pic_id VARCHAR,
   reviews VARCHAR []
);

-- review_id will be stored in reviews array, see review table below
   
CREATE TABLE location (
   user_id VARCHAR (30) PRIMARY KEY,
   address VARCHAR (100),
   city VARCHAR (40),
   state VARCHAR (2),
   zip VARCHAR (5),
   coordinates geometry(Point,4326)
);
-- point example, '(-3.1883, 55.9533)' which is (longitude, latitude)

CREATE TABLE job_post (
   job_id VARCHAR (30) PRIMARY KEY,
   poster_id VARCHAR (64) NOT NULL REFERENCES users (username) ON DELETE CASCADE,
   worker_id VARCHAR (64) REFERENCES users (username) ON DELETE CASCADE,
   post_date DATE,
   post_time timestamp,
   completed_date DATE,
   job_desc VARCHAR,
   price VARCHAR (6),
   chat_ids VARCHAR [],
   compl_message VARCHAR (256),
   reject_message VARCHAR (256),
   is_privet BOOLEAN,
   needs_compl_photo BOOLEAN DEFAULT FALSE,
   needs_completion BOOLEAN DEFAULT FALSE, 
   view_rad VARCHAR (2),
   skills VARCHAR [],
   jobCategories VARCHAR [],
   applied_worker VARCHAR [],
   title VARCHAR (100)
);

CREATE TABLE worker (
   user_id VARCHAR (30) PRIMARY KEY,
   skills VARCHAR [],
   payrate SMALLINT,
   job_hist VARCHAR [],
   workRadius VARCHAR (2),
   jobCategories VARCHAR [],
   avgRating NUMERIC (4,3)
);

CREATE TABLE review (
   review_id VARCHAR (30) PRIMARY KEY,
   user_id VARCHAR (30),
   reviewed_by VARCHAR (30),
   rev_desc VARCHAR,
   rating VARCHAR (1),
   rev_date DATE,
   job_ref VARCHAR (30)
)

-- review_id: uniquie id of the review, use uniqid("rev"); to generate unique id. 
--    Also stored in reviews arrary of users table, in the row of the user being reviewed.
-- user_id: the user_id/username of the user being reviewed
-- reviewed_by: the user_id/username of the user making the review
-- rev_desc: description message of the review
-- rating: numeric 1-5 rating value 
-- rev_date: date when the review was made
-- job_ref: job_id of reviewed job, 
--    from job_id we can compare poster_id and worker_id 
--    to determine roll of reviewed user in job (worker or poster). May not be used
